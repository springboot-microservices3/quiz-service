package com.inapp.quiz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.inapp.quiz.model.Quiz;

public interface QuizRepository extends JpaRepository<Quiz, Integer> {

}
