package com.inapp.quiz.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inapp.quiz.model.QuestionWrapper;
import com.inapp.quiz.model.QuizDTO;
import com.inapp.quiz.model.Response;
import com.inapp.quiz.service.QuizService;

@RestController
@RequestMapping("quiz")
public class QuizController {
	
	@Autowired
	QuizService quizService;
	
	@PostMapping("create")
	private ResponseEntity<String> createQuiz(@RequestBody QuizDTO quizDto)
	{
		return quizService.createQuiz(quizDto);
	}
	
	@GetMapping("get/{id}")
	private ResponseEntity<List<QuestionWrapper>> getQuestionsForQuiz(@PathVariable int id)
	{
		return quizService.getQuestions(id);
	}
	
	@PostMapping("getresult")
	private ResponseEntity<Integer> getScore(@RequestBody List<Response> responses)
	{
		return quizService.getTotalScore(responses);
	}

}
