package com.inapp.quiz.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.inapp.quiz.feign.QuizInterface;
import com.inapp.quiz.model.QuestionWrapper;
import com.inapp.quiz.model.Quiz;
import com.inapp.quiz.model.QuizDTO;
import com.inapp.quiz.model.Response;
import com.inapp.quiz.repository.QuizRepository;

@Service
public class QuizService {
	
	@Autowired
	QuizRepository quizRepo;
	
	@Autowired
	QuizInterface quizInterface;

	public ResponseEntity<String> createQuiz(QuizDTO quizDto) {
		try
		{
			List<Integer> questionIdsList =  quizInterface.generateQuestionsByCategory(quizDto.getCategory(), quizDto.getNoOfQuestions()).getBody();
			Quiz quiz = new Quiz();
			quiz.setTitle(quizDto.getTitle());
			quiz.setCategory(quizDto.getCategory());
			quiz.setNoOfQuestions(quizDto.getNoOfQuestions());
			quiz.setQuestionIds(questionIdsList);
			quizRepo.save(quiz);
			return new ResponseEntity<>("Quiz created successfully",org.springframework.http.HttpStatus.CREATED);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>("Failed, please check server logs", org.springframework.http.HttpStatus.NOT_FOUND);
	}

	public ResponseEntity<List<QuestionWrapper>> getQuestions(int id) {
		try
		{
			Quiz quiz = quizRepo.findById(id).get();
			List<Integer> questionIds=quiz.getQuestionIds();
			List<QuestionWrapper> questions = quizInterface.getQuestionsById(questionIds).getBody();
			return new ResponseEntity<>(questions,org.springframework.http.HttpStatus.OK);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(new ArrayList<>(), org.springframework.http.HttpStatus.NOT_FOUND);
	}

	public ResponseEntity<Integer> getTotalScore(List<Response> responses) {
		try
		{
			int score=quizInterface.getScore(responses).getBody();
			return new ResponseEntity<>(score,org.springframework.http.HttpStatus.OK);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(null, org.springframework.http.HttpStatus.NOT_FOUND);
	}

}
