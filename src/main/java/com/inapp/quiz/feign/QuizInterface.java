package com.inapp.quiz.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.inapp.quiz.model.QuestionWrapper;
import com.inapp.quiz.model.Response;


@FeignClient("QUESTION-SERVICE")
public interface QuizInterface {
	
	@GetMapping("question/generate")
	public ResponseEntity<List<Integer>> generateQuestionsByCategory(@RequestParam String category,@RequestParam int noOfQuestions);
	
	@PostMapping("question/get")
	public ResponseEntity<List<QuestionWrapper>> getQuestionsById(@RequestBody List<Integer> questionIdList);
	
	@PostMapping("question/getscore")
	public ResponseEntity<Integer> getScore(@RequestBody List<Response> responses);
}
